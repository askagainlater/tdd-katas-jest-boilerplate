import axios from "axios";
import { FaxValidator, FaxValidatorType } from "./fax-validator";

export interface IFax {
  title: string;
  recipient: string;
  body: string;
}

export class FaxService {
  private _db: Map<number, IFax>;
  private _validator: FaxValidatorType;
  constructor(db: Map<number, IFax>, faxValidator: FaxValidatorType) {
    this._db = db;
    this._validator = faxValidator;
  }
  async getFax(id: number): Promise<IFax | null> {
    return axios.get(`/fax/${id}`).then((resp) => resp.data);
  }

  async getAll(): Promise<IFax[] | null> {
    return axios.get("/fax").then((resp) => resp.data);
  }

  async sendFax(fax: IFax): Promise<boolean> {
    if (this._validator.a.b.c.isValid(fax)) {
      return true;
    } else {
      throw new Error("Invalid Fax");
    }
  }

  validateFax(fax: IFax): boolean {
    return this._validator.a.b.c.isValid(fax);
  }
}
