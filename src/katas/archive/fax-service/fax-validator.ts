import { IFax } from "./fax-service";

export type FaxValidatorType = {
  a: { b: { c: { isValid: (fax: IFax) => boolean } } };
};
export const FaxValidator: FaxValidatorType = {
  // Unnecessarily deep nested object
  a: {
    b: {
      c: {
        isValid: (fax: IFax) => fax?.recipient != null,
      },
    },
  },
};
