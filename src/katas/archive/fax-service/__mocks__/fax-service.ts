import { IFax } from "../fax-service";
import { FaxValidator, FaxValidatorType } from "../fax-validator";

export const FAX_FAKE_DB: Map<number, IFax> = new Map();
export const FAX_FAKE_1 = {
  title: "Test-Fax-1",
  recipient: "+490000001",
  body: "Test-Fax 1 Body",
};
export const FAX_FAKE_2 = {
  title: "Test-Fax-2",
  recipient: "+490000002",
  body: "Test-Fax 2 Body",
};
export const FAX_FAKE_3 = {
  title: "Test-Fax-3",
  recipient: "+490000003",
  body: "Test-Fax 3 Body",
};

export class FaxService {
  private _db: Map<number, IFax>;
  private _validator: FaxValidatorType;
  constructor(db: Map<number, IFax>, validator: FaxValidatorType) {
    this._db = db;
    this._validator = validator;
  }

  async getFax(id: number): Promise<IFax | null> {
    const fakeFax = FAX_FAKE_DB.get(id);
    return new Promise<IFax | null>((resolve, reject) => {
      process.nextTick(() => {
        if (fakeFax != null) {
          resolve(fakeFax);
        } else {
          reject(new Error("Not Found"));
        }
      });
    });
  }

  async getAll(): Promise<IFax[] | null> {
    return new Promise<IFax[] | null>((resolve, reject) => {
      process.nextTick(() => {
        resolve([FAX_FAKE_1, FAX_FAKE_2, FAX_FAKE_3]);
      });
    });
  }

  async sendFax(fax: IFax): Promise<boolean> {
    if (true) {
      return true;
    } else {
      throw new Error("Invalid Fax");
    }
  }

  validateFax(fax: IFax): boolean {
    return this._validator.a.b.c.isValid(fax);
  }
}
