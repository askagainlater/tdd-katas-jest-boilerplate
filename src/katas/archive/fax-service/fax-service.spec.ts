import {
  FAX_FAKE_1,
  FAX_FAKE_2,
  FAX_FAKE_3,
  FAX_FAKE_DB,
} from "./__mocks__/fax-service";
import { FaxService } from "./fax-service";
import { FaxValidator, FaxValidatorType } from "./fax-validator";
import axios from "axios";
import { mocked } from "ts-jest/dist/utils/testing";
jest.mock("./fax-validator");

describe("FaxService", () => {
  describe("Kata 1: Testing Asynchronous Code", () => {
    jest.mock("./fax-service");
    let faxService: FaxService;
    beforeAll(() => {
      // MANUAL MOCK, see __mocks__/ (https://jestjs.io/docs/manual-mocks)
      FAX_FAKE_DB.set(1, FAX_FAKE_1);
      FAX_FAKE_DB.set(2, FAX_FAKE_2);
      FAX_FAKE_DB.set(3, FAX_FAKE_3);
      faxService = new FaxService(FAX_FAKE_DB, FaxValidator);
    });

    it("returns a fax if getFax is called with known id 1", () => {
      // TODO: use manual mock
    });

    it("getFax rejects with error 'Not Found' if a fax with unknown id -1 is requested", () => {
      // TODO: use manual mock
    });

    afterAll(() => {
      jest.clearAllMocks();
    });
  });

  describe("Kata 2: Mock Functions", () => {
    beforeEach(() => {
      jest.resetAllMocks();
    });

    it("returns a fax if getFax is called with known id 1", async () => {
      // TODO: use mock objects / spies
    });

    it("axios.get has been called exactly once if getFax is used", async () => {
      // TODO: use mock objects / spies
    });

    it("Fax is validated before it is sent", async () => {
      // TODO: use mock objects / spies
    });

    afterAll(() => {
      jest.clearAllMocks();
    });
  });
});
