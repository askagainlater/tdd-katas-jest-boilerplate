export class LinkedList {
  /**
    * https://ccd-school.de/coding-dojo/class-katas/linked-list/
    * 
    * Entwickle den abstrakten Datentyp Liste in Form 
    * einer verketteten Liste. Die zu implementierende Klasse 
    * LinkedList<T> muss das Interface IList<T> implementieren. 
    * Eine verkettete Liste besteht aus Elementen, 
    * die jeweils einen Wert sowie einen Zeiger auf das nächste Element der Liste enthalten:
    * 
    * class Element<T> {
    *   public Element(T item) {
            Item = item;
    *   }
    *
    *   public T Item { get; set; }
    *
    *   public Element<T> Next { get; set; }
    * }
    */
}
