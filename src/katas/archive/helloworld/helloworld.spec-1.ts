import { HelloWorld } from "./helloworld";

describe("HelloWorld", () => {
  describe("sayHello", () => {
    test("it returns 'Hello World'", () => {
      const output: string = new HelloWorld().sayHello();
      expect(output).toEqual("Hello World");
    });
  });
});
