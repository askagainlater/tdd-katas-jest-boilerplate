import { Rot13 } from "./rot13";

describe("ROT-13 Kata", () => {
  // TODO
  it("H zu U", () => {
    expect(crypt("h")).toBe("U");
  });
  it("ä zu NR", () => {
    expect(crypt("ä")).toBe("NR");
  });
  it("hello zu URYYB", () => {
    expect(crypt("hello")).toBe("URYYB");
  });
});

// const abc = "ABC"
// const rotABC = "NOPQ"

function crypt(value: string): string {
  let output = "";
  let input = value.toUpperCase();
  const abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
  const umlautsMap: { [key: string]: string } = {
    Ä: "AE",
    Ö: "OE",
    Ü: "UE",
    ß: "SS",
  };
  const umlaut = umlautsMap[input];
  if (umlaut != null) {
    input = umlaut;
  }
  for (let i = 0; i < input.length; i++) {
    var index = abc.indexOf(input[i]);
    index += 13;
    if (index > abc.length) {
      index -= abc.length;
    }
    output += abc[index];
  }

  return output;
}
