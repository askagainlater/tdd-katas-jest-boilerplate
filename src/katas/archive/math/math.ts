export class MathUtil {
  add(original: number, toAdd: number): number {
    return this._sum(original, toAdd);
  }

  _sum(input1: number, input2: number): number {
    return input1 + input2;
  }
}
