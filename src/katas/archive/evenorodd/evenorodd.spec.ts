function evenOrOdd(num: number | null): string {
  if (num === null) {
    return "invalid number";
  }
  if (num % 2 > 0) {
    return "odd";
  }
  if (num % 2 === 0) {
    return "even";
  }
  return "";
}

describe("even or odd", () => {
  it("should return 'invalid number' on null", () => {
    expect(evenOrOdd(null)).toBe("invalid number");
  });
  it("should return 'odd' on 1", () => {
    expect(evenOrOdd(1)).toBe("odd");
  });
  it("should return 'even' on 2", () => {
    expect(evenOrOdd(2)).toBe("even");
  });
  it("should return 'odd' on 3", () => {
    expect(evenOrOdd(3)).toBe("odd");
  });
  it("should return 'odd' on 667", () => {
    expect(evenOrOdd(667)).toBe("odd");
  });
});
