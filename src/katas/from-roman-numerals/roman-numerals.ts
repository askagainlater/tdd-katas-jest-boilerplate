export class RomanNumerals {
  /**
   * https://ccd-school.de/coding-dojo/function-katas/from-roman-numerals/
   *
   * Schreibe eine Funktion, die Römische Zahlen [1] in Dezimalzahlen übersetzt.
   * Beispiele:
   * „I“ -> 1
   * „II“ -> 2
   * „IV“ -> 4
   * „V“ -> 5
   * „IX“ -> 9
   * „XLII“ -> 42
   * „XCIX“ -> 99
   * „MMXIII“ -> 2013
   * Die Römischen Zahlen bewegen sich im Bereich von „I“ bis „MMM“.
   */

  static convert(input: string): number {
    let returnValue = 0;

    for (let i = 0; i < input.length; i++) {
      if (input[i] === "I") {
        if (input[i + 1] && (input[i + 1] ===  "V" || input[i + 1] ===  "X")) {
          returnValue -= 1;
        } else {
          returnValue = returnValue + 1;
        }
      }
      if (input[i] === "X") {
        if (input[i + 1] && (input[i + 1] ===  "L" || input[i + 1] ===  "C")) {
          returnValue -= 10;
        } else {
          returnValue = returnValue + 10;
        }
      }

      if (input[i] === "C") {
        if (input[i + 1] && (input[i + 1] ===  "D" || input[i + 1] ===  "M")) {
          returnValue -= 100;
        } else {
          returnValue = returnValue + 100;
        }
      }

      if (input[i] === "V") {
        returnValue = returnValue + 5;
      }
      if (input[i] === "L") {
        returnValue = returnValue + 50;
      }
      if (input[i] === "D") {
        returnValue = returnValue + 500;
      }
      if (input[i] === "M") {
        returnValue = returnValue + 1000;
      }
    }
    return returnValue;
  }
}
