import { RomanNumerals } from "./roman-numerals";


    /**
     * https://ccd-school.de/coding-dojo/function-katas/from-roman-numerals/
     * 
     * Schreibe eine Funktion, die Römische Zahlen [1] in Dezimalzahlen übersetzt.
     * Beispiele:
     * „I“ -> 1
     * „II“ -> 2
     * „IV“ -> 4
     * „V“ -> 5
     * „IX“ -> 9
     * „XLII“ -> 42
     * „XCIX“ -> 99
     * „MMXIII“ -> 2013
     * Die Römischen Zahlen bewegen sich im Bereich von „I“ bis „MMM“.
     */

describe("From Roman Numerals Kata", () => {
  // TODO
  it("should convert I to 1", () => {
    const result = RomanNumerals.convert("I");
    expect(result).toBe(1);
    
  });

  it("should convert II to 2", () => {
    const result = RomanNumerals.convert("II");
    expect(result).toBe(2);
  });

  it("should convert III to 3", () => {
    const result = RomanNumerals.convert("III");
    expect(result).toBe(3);
  });

  it("should convert IV to 4", () => {
    const result = RomanNumerals.convert("IV");
    expect(result).toBe(4);
  });

  it("should convert V to 5", () => {
    const result = RomanNumerals.convert("V");
    expect(result).toBe(5);
  });

  it("should convert VI to 6", () => {
    const result = RomanNumerals.convert("VI");
    expect(result).toBe(6);
  });

  it("should convert IX to 9", () => {
    const result = RomanNumerals.convert("IX");
    expect(result).toBe(9);
  });

  it("should convert XX to 20", () => {
    const result = RomanNumerals.convert("XX");
    expect(result).toBe(20);
  });

  it("should convert XXVII to 27", () => {
    const result = RomanNumerals.convert("XXVII");
    expect(result).toBe(27);
  });

    it("should convert XLII to 42", () => {
    const result = RomanNumerals.convert("XLII");
    expect(result).toBe(42);
  });

    it("should convert XLVIII to 48", () => {
    const result = RomanNumerals.convert("XLVIII");
    expect(result).toBe(48);
  });
    it("should convert XCIX to 99", () => {
    const result = RomanNumerals.convert("XCIX");
    expect(result).toBe(99);
  });

  it("should convert MMXIII to 2013", () => {
    const result = RomanNumerals.convert("MMXIII");
    expect(result).toBe(2013);
  });

  it("should convert MCMLVI to 1956", () => {
    const result = RomanNumerals.convert("MCMLVI");
    expect(result).toBe(1956);
  });

  it("should convert CDXX to 420", () => {
    const result = RomanNumerals.convert("CDXX");
    expect(result).toBe(420);
  });
});