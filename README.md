## Get Started

- `npm install`
- `npm run test:watch`
- Put new Katas and tests into `src\kataname\classname.ts` and `src\kataname\classname.spec.ts`

## Find Katas

- https://project-awesome.org/gamontal/awesome-katas
- https://kata-log.rocks/tdd.html

## Katas 2022/02/28

- ROT-13 https://ccd-school.de/coding-dojo/function-katas/rot-13/
- Linked List https://ccd-school.de/coding-dojo/class-katas/linked-list/

## Test naming suggestion

**unit of work - scenario/context - expected behaviour**

```typescript
describe("[unit of work]", () => {
  it("should [expected behaviour] when [scenario/context]", () => {});
});
```

```typescript
describe("[unit of work]", () => {
  describe("when [scenario/context]", () => {
    it("should [expected behaviour]", () => {});
  });
});
```

## Further Reading

https://github.com/mawrkus/js-unit-testing-guide
